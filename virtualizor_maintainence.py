import sys
import os
import logging
import subprocess


# Worker script to perform maintainence checks
# Status codes - 
# -1 - Test failed
# >0 - Test passed
#  0 - Test not started


# Tests to conduct
tests = ["resolvconf_solvers", "updates_check", "virtnetwork_check", "virtualizor_runcron"]

# Default value of each test
statuses = dict(zip(tests, [0 for i in range(len(tests))]))

def check_ping(hostname):
    response = os.system("ping -c 1 " + hostname)

    if response == 0:
        return True
    return False

def resolvconf_solvers(rconfpath = '/etc/resolv.conf', invalid_serverlist = ["127.0.0.1"]):

    logging.info("Checking " + str(rconfpath) + " for valid nameservers...")

    resolveconf = open(rconfpath, "r")
    lines = resolveconf.readlines()
    nameserver_present = False

    for line in lines:
        if line.startswith("nameserver"):
            # Grab the nameserver
            ns = list(filter(None, line.split(" ")))[-1][:-1]

            logging.info("Found nameserver " + str(ns))
            logging.info("Testing ping...")

            if ns not in invalid_serverlist and check_ping(ns):
                # Incriment the number of valid resolvers
                statuses['resolvconf_solvers'] += 1
    
    resolveconf.close()

    # Check if test passed
    if statuses['resolvconf_solvers'] > 0:
        logging.info("Found", statuses['resolvconf_solvers'], "nameservers!")
        return True

    logging.error("Found no valid nameservers! Please edit " + rconfpath + " yourself!")
    cmd = "sudo nano " + rconfpath
    subprocess.call(cmd.split())
    statuses['resolvconf_solvers'] = -1
    return False

def updates_check():

    logging.info("Checking if any available updates...")
    
    rval = os.system("yum check-update")

    # If we have any updates, run yum update and let the user decide
    if rval > 0:

        # Test has passed
        logging.info("Got return value 100, running yum update...")
        statuses['updates_check'] += rval

    elif rval == 0:

        # Test has passed
        statuses['updates_check'] = 1
        logging.info("No updates found, moving on...")
    elif rval == -1:

        # Test has failed
        statuses['updates_check'] = -1
        logging.warning("Some error occured with yum check-update, please investigate...")
    else:

        # Test has failed
        statuses['updates_check'] = -1
        logging.warning("Unknown return value "+ str(rval) +", skipping test")

    logging.info("Finished test updates_check")

    if statuses['updates_check'] > 0:
        return True
    
    return False


def virtnetwork_check():

    logging.info("Checking virtnetwork status...")

    # TODO: Complete this damn function
    cmdline = "service virtnetwork status".split()
    success_output = b'viifbr0 is running \n'

    # Grab this command's output
    output = subprocess.run(cmdline, stdout=subprocess.PIPE).stdout

    print(output)

    if output == success_output:
        statuses['virtnetwork_check'] = 1
    else:
        logging.error("virtnetwork returned error status! Please restart virtnetwork")
        statuses['virtnetwork_check'] = -1

    if statuses['virtnetwork_check'] > 0:
        return True
    
    return False
        
def virtualizor_runcron():

    logging.info("Running virtualizor cronjob...")

    os.system("/usr/local/emps/bin/php /usr/local/virtualizor/scripts/cron.php")
    statuses["virtualizor_runcron"] = 1

    return True

if __name__ == "__main__":
    try:
        loglevel = sys.argv[sys.argv.index("--log") +1]
        logging.basicConfig(level = eval("logging." + loglevel))
    except ValueError:
        logging.basicConfig(level = logging.WARN)
    
    # Run tests

    for testname in tests:
        eval(testname+"()")
    
    print("==========================")
    print(statuses)
    print("==========================")